const myCheck = document.querySelector('.btn_mycheck'),
  modMyCheck = document.querySelector('.modal_check'),
  btnNewCheck = document.querySelector('.new_check'),
  mod = document.querySelector('.modal'),
  errorMod = document.querySelector('.error'),
  add = document.querySelector('.add')
  modlId = document.querySelector('.modal_id'),
  bodyStop = document.querySelector('.stop'),
  close = document.querySelectorAll('.closed');

myCheck.addEventListener('click', function(e){
  e.preventDefault(),
  modlId.classList.add('show'),
  bodyStop.classList.add('noscroll');
});

btnNewCheck.addEventListener('click', function(e){
  e.preventDefault(),
  mod.classList.add('show'),
  bodyStop.classList.add('noscroll');
});


close.forEach(function(item) {
  item.addEventListener('click', function(e) {
    modMyCheck.classList.remove('show'),
    mod.classList.remove('show'),
    errorMod.classList.remove('show'),
    modlId.classList.remove('show'),
    bodyStop.classList.remove('noscroll');
  });
});